# do not forget the namespace here
defmodule Maplog.CLI do
  require Logger

  @spec stream_lines(Enumerable.t()) :: Enumerable.t()
  def stream_lines(in_stream) do
    in_stream |> Stream.map(&Maplog.parse_map(Jason.decode!(&1)))
  end

  def main(_args \\ []) do
    stdio = IO.stream(:stdio, :line)

    stdio
    |> stream_lines()
    |> Stream.into(stdio, fn x -> "#{x}\n" end)
    |> Stream.run()

    ## Logger.info("Saving registry..")
    ## Process.sleep(5000)
    ## Maplog.save
  end
end
