defmodule Maplog.Registry do
  use GenServer

  require Logger
  ## Client API

  @doc """
  Starts the registry.
  """

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    store = load()
    {:ok, store}
  end

  def load() do
    case File.read(Application.get_env(:maplog, :registry_file)) do
      {:ok, body} ->
        store_decode(body)

      {:error, reason} ->
        Logger.error(reason)
        %{}
    end
  end

  defp store_decode(store) do
    store
    |> Jason.decode!()
    |> Map.new(fn {k, v} -> {k, MapSet.new(v)} end)
  end

  defp store_encode(store) do
    store
    |> Map.new(fn {k, v} -> {k, MapSet.to_list(v)} end)
    |> Jason.encode!()
  end

  defp save(store) do
    filename = Application.get_env(:maplog, :registry_file)
    # export_store = :erlang.term_to_binary(store)
    case File.write(filename, store_encode(store), [:utf8]) do
      :ok -> :ok
      {:error, reason} -> Logger.error(reason)
    end
  end

  def add_value(key, value), do: GenServer.cast(__MODULE__, {:add_value, key, value})
  def delete_key(key), do: GenServer.cast(__MODULE__, {:delete, key})
  def delete_value(key, value), do: GenServer.cast(__MODULE__, {:delete_value, key, value})
  def dump, do: GenServer.call(__MODULE__, :dump)
  def get_keys, do: GenServer.call(__MODULE__, :get_keys)
  def get_key_values(key), do: GenServer.call(__MODULE__, {:get_key_values, key})
  def empty, do: GenServer.cast(__MODULE__, :empty)
  def save, do: GenServer.cast(__MODULE__, :save)

  def handle_call(:dump, _from, store) do
    {:reply, store, store}
  end

  def handle_call(:get_keys, _from, store) do
    {:reply, Map.keys(store), store}
  end

  def handle_call({:get_key_values, key}, _from, store) do
    {:reply, Map.fetch(store, key), store}
  end

  def handle_cast({:add_value, key, value}, store) do
    store_new =
      case Map.has_key?(store, key) do
        true ->
          if MapSet.member?(store[key], value) do
            Logger.debug("Value " <> key <> "/" <> value <> " already inserted!")
            store
          else
            Logger.info("Adding a new value " <> key <> "/" <> value)
            %{store | key => s = MapSet.put(store[key], value)}
            Task.async(fn -> save(s) end)
            s
          end

        false ->
          Logger.info("Adding a new key/value " <> key <> "/" <> value)
          s = Map.put(store, key, MapSet.new([value]))
          Task.async(fn -> save(s) end)
          s
      end

    {:noreply, store_new}
  end

  def handle_cast({:delete_key, key}, store) do
    store_new = Map.delete(store, key)
    save(store_new)
    {:noreply, store_new}
  end

  def handle_cast({:delete_value, key, value}, store) do
    store_new = %{store | key => MapSet.delete(store[key], value)}
    save(store_new)
    {:noreply, store_new}
  end

  def handle_cast(:empty, _store) do
    save(%{})
    {:noreply, %{}}
  end

  def handle_cast(:save, store) do
    save(store)
    {:noreply, store}
  end

  def handle_info({:DOWN, _ref, :process, _pid, _reason}, store) do
    save(store)
    {:noreply, store}
  end

  def handle_info(_msg, state) do
    {:noreply, state}
  end
end
